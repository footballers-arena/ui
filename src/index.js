import React from 'react';
import ReactDOM from 'react-dom';
import {Provider as ReduxProvider} from 'react-redux'

import './index.scss';


import App from './app';
import configureStore from './lib/store';
import registerServiceWorker from './registerServiceWorker';

import DevTools from './app/DevTools';


require('typeface-roboto');
const store = configureStore();

if (process.env.NODE_ENV === 'production') {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <div className="app">
        <App />
      </div>
    </ReduxProvider>,
  document.getElementById('root'));
} else {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <div className="app">
        <App />
        <DevTools/>
      </div>
    </ReduxProvider>,
  document.getElementById('root'));
}
registerServiceWorker();
