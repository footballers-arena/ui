function removeAccents(str) {
  return str.replace(/[^a-zA-Z- ]/g, "");
}

let baseUrl;

if (process.env.NODE_ENV === 'production') {
  baseUrl = "";
} else {
  baseUrl = "http://localhost:3000";
}

module.exports = {
  removeAccents,
  baseUrl
}
