import {combineReducers} from 'redux';

import {reducer as footballers} from './footballers';
import {reducer as login} from './login';

export default combineReducers({
    footballers,
    login
});
