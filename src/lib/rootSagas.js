import {fork} from 'redux-saga/effects'

import {footballersSaga} from './footballers';
import {loginSaga} from './login';

export default function* rootSaga() {
    yield fork(footballersSaga);
    yield fork(loginSaga);
}
