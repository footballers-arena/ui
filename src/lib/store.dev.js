import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import DevTools from '../app/DevTools';
import rootReducer from './rootReducer';
import rootSaga from './rootSagas';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const store = createStore(
    rootReducer,
    {},
    compose(
      applyMiddleware(sagaMiddleware),
      DevTools.instrument()
    )
  )

  sagaMiddleware.run(rootSaga)

  return store;
}
