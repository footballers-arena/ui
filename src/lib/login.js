import {put, call, takeLatest} from 'redux-saga/effects';
import {createAction, createReducer} from 'redux-act';
import axios from 'axios';
import {baseUrl} from '../utils';

axios.defaults.withCredentials = true;

export const login = createAction("Login Action");
export const ping = createAction("Ping Action");
export const register = createAction("Register User Action");
export const logout = createAction("Logout Action");
export const login_success = createAction("Auth action Success");
export const login_failure = createAction("Auth action Failure");
export const clear_all = createAction("Clear All");

export const reducer = createReducer({
    [login_success]: (state, user) => ({user}),
    [login_failure]: (state, error) => ({error})
}, {
    user: null,
    error: null,
});

function* loginAsync(action) {
    try {
        debugger;
        const { data } = yield call(axios.post, `${baseUrl}/login`, action.payload);
        yield put(login_success(data));
    } catch(err) {
        yield put(login_failure({ login: err.response.data || err }));
    }
}

function* pingAsync(action) {
    try {
        const {data} = yield call(axios.get, `${baseUrl}/ping`);
        yield put(login_success(data));
    } catch(err) {
        //yield put(login_failure(err));
    }
}

function* logoutAsync(action) {
    try {
        yield call(axios.get, `${baseUrl}/logout`);
        yield put(login_success(null));
        yield put(clear_all());
    } catch(err) {
        yield put(login_failure(err.response.data || err));
    }
}

function* registerAsync(action) {
    try {
        const {data} = yield call(axios.post, `${baseUrl}/register`, action.payload);
	yield put(login_success(data));
    } catch(err) {
        yield put(login_failure({ register: err.response.data || err }));
    }
}

export function* loginSaga() {
    yield takeLatest(login.getType(), loginAsync);
    yield takeLatest(logout.getType(), logoutAsync);
    yield takeLatest(register.getType(), registerAsync);
    yield takeLatest(ping.getType(), pingAsync);
}
