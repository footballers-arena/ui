import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './rootReducer';
import rootSaga from './rootSagas';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const store = createStore(
    rootReducer,
    {},
    compose(
      applyMiddleware(sagaMiddleware),
    )
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
