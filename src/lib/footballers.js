import {put, call, takeLatest} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import {createAction, createReducer} from 'redux-act';
import axios from 'axios';
import {baseUrl} from '../utils';
import {clear_all} from './login.js';
import {remove} from 'lodash';

export const fetch_all = createAction("Action fetches all footballers");
export const fetch_selected = createAction("Action to fetch single footballer");
export const fetch_all_succeeded = createAction("Action fetch succeeded");
export const fetch_succeeded = createAction("Action fetch succeeded");
export const fetch_failed = createAction("Action fetch failed");
export const clear_selected = createAction("Clears Selected");
export const search = createAction("Search");
export const sort = createAction("Sort");
export const group = createAction("Group");
export const sortOrder = createAction("Sort Order");
export const toggleCollapse = createAction("Toggle Collapse");
export const set_search_text = createAction("Sets seacrh text");
export const fetch_favourites = createAction("Fetches favourites");
export const set_favourite = createAction("Sets favourite");
export const rem_favourite = createAction("Removes favourite");
export const toggleOnlyFavourites = createAction("Toggle Only Favourites");
export const favourites =  createAction("Simpple action to set favourites in store");

const defaultState = {
    error: null,
    list: [],
    selected: null,
    searchText: '',
    groupBy: 'None',
    sortBy: 'Rating',
    sortOrder: 'd',
    collapsedGroups: [],
    favourites: [],
    onlyFavourites: false
};

export const reducer = createReducer({
  [fetch_all_succeeded]: (state, list) => Object.assign({}, state, {list, error: null}),
  [fetch_failed]: (state, error) => Object.assign({}, state, {error}),
  [fetch_succeeded]: (state, selected) => Object.assign({}, state, {selected}),
  [clear_selected]: (state) => Object.assign({}, state, {selected: null}),
  [set_search_text]: (state, searchText) => Object.assign({}, state, {searchText}),
  [sort]: (state, sortBy) => Object.assign({}, state, {sortBy}),
  [group]: (state, groupBy) => Object.assign({}, state, {groupBy}),
  [sortOrder]: (state, sortOrder) => Object.assign({}, state, {sortOrder}),
  [favourites]: (state, favourites) => Object.assign({}, state, {favourites}),
  [toggleOnlyFavourites]: (state) => Object.assign({}, state, {onlyFavourites: !state.onlyFavourites}),
  [toggleCollapse]: (state, elem) => {
  	const collapsedGroups = Object.assign([], state.collapsedGroups);
  	if (collapsedGroups.includes(elem)) {
  		remove(collapsedGroups, x => x === elem);
  	} else {
  		collapsedGroups.push(elem);
  	}
  	return Object.assign({}, state, {collapsedGroups})
  },
    [clear_all]: () => Object.assign({}, defaultState)
}, defaultState);

function* fetchFootballers(action) {
  try {
    const { data } = yield call(axios.get, `${baseUrl}/footballers`, {
      params: {
        filter: action.payload
      }
    });
    yield put(fetch_all_succeeded(data));
  } catch (error) {
    yield put(fetch_failed(error));
  }
}

function* fetchSelected(action) {
  try {
    if (action.payload) {
        yield put(fetch_succeeded(action.payload));
    }
    const { data } = yield call(axios.get, `${baseUrl}/footballers/${action.payload.id}`)
    yield put(fetch_succeeded(data))
  } catch (error) {
    yield put(fetch_failed(error));
  }
}

function* searchFootballers(action) {
  try {
    yield put(set_search_text(action.payload));
    yield call(delay, 500)
    if (action.payload) {
        yield put(fetch_all(action.payload));
    } else {
      yield put(fetch_all());
    }
  } catch (error) {
    yield put(fetch_failed(error));
  }
}

function* fetchFavourites(action) {
	try {
	    const { data } = yield call(axios.get, `${baseUrl}/favourites`);
	    yield put(favourites(data));
	  } catch (error) {
	    yield put(fetch_failed(error));
	  }
}

function* setFavourite(action) {
	try {
		if (action.payload) {
	    	const { data } = yield call(axios.put, `${baseUrl}/favourites/${action.payload}`);
		    yield put(favourites(data));
	    }
	  } catch (error) {
	    yield put(fetch_failed(error));
	  }
}

function* unsetFavourite(action) {
	try {
		if (action.payload) {
	    	const { data } = yield call(axios.delete, `${baseUrl}/favourites/${action.payload}`);
		    yield put(favourites(data));
	    }
	  } catch (error) {
	    yield put(fetch_failed(error));
	  }
}


export function* footballersSaga() {
  yield takeLatest(fetch_all.getType(), fetchFootballers);
  yield takeLatest(fetch_selected.getType(), fetchSelected);
  yield takeLatest(search.getType(), searchFootballers);
  yield takeLatest(fetch_favourites.getType(), fetchFavourites);
  yield takeLatest(set_favourite.getType(), setFavourite);
  yield takeLatest(rem_favourite.getType(), unsetFavourite);
}
