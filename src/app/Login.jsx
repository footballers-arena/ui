import React from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import Helmet from 'react-helmet';

import {ping, login, logout, register} from '../lib/login';

import './Login.scss';

class Login extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.setText = this.setText.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onRegister = this.onRegister.bind(this);
        this.state = {
        	Username: '',
        	Password: '',
			Name: '',
			Email: ''
        }
    }

    setText(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    onSubmit(event) {
        this.props.login({
            Username: this.state.Username,
            Password: this.state.Password
        });
	event.preventDefault();
    }
    onRegister(event) {
        this.props.register({
            Username: this.state.Username,
            Password: this.state.Password,
            Name: this.state.Name,
            Email: this.state.Email
        });
	event.preventDefault();
    }
    componentDidMount() {
	this.props.ping();	
    }
    static getDerivedStateFromProps(nextProps, prevState) {
	const isLogin = nextProps && nextProps.location && nextProps.location.pathname.split("/")[1] === "login";
	if (!prevState) {
		return {
			Username: '',
			Password: '',
			Name: '',
			Email: '',
			isLogin
		};
	}
	return {
		isLogin
	};
    }
    render() {
    console.log(JSON.stringify(this.props));
	if (!!this.props.user) {
		return <Redirect to="/" />;
	}
	const isLogin = this.state.isLogin;
        return (
	  <div className="loginAndReg">
	    { this.props.error && !(this.props.loginError || this.props.registerError) && 
	    <p>"Server Error  - Contact Dev"</p>
	    }
	    {isLogin ? (
            <div className="loginContainer">
            		<Helmet>
            			<title>Login</title>
            		</Helmet>
		<form onSubmit={this.onSubmit}>
		    <label>
			Username:
		    </label>
			<input
			    name="Username"
			    value={this.state.Username}
			    className="Username"
			    type="text"
			    onChange={this.setText}
			/>
		    <label>
			Password:
		    </label>
			<input
			    name="Password"
			    value={this.state.Password}
			    className="Password"
			    type="Password"
			    onChange={this.setText}
			/>
			<input type="submit" value="Login"/>
		</form>
    {this.props.loginError && <p>Invalid Username/Password</p>}
		    <Link to="/register">Sign up</Link>
	     </div>) : (
	     <div className="registrationContainer">
	     		<Helmet>
	     			<title>Register</title>
	     		</Helmet>
		<form onSubmit={this.onRegister}>
		    <label>
			     Email:
                   {this.props.registerError && this.props.registerError.Email && <p className="formError">{this.props.registerError.Email}</p>}
		    </label>
			<input
			    name="Email"
			    value={this.state.Email}
			    className="Email"
			    type="text"
			    onChange={this.setText}
			/>
		    <label>
			      Name:
            {this.props.registerError && this.props.registerError.Name && <p className="formError">{this.props.registerError.Name}</p>}
		    </label>
			<input
			    name="Name"
			    value={this.state.Name}
			    className="Name"
			    type="text"
			    onChange={this.setText}
			/>
		    <label>
		        Username:
            {this.props.registerError && this.props.registerError.Username && <p className="formError">{this.props.registerError.Username}</p>}
		    </label>
			<input
			    label="Username"
			    name="Username"
			    value={this.state.Username}
			    className="Username"
			    type="text"
			    onChange={this.setText}
			/>
		    <label>
		        Password:
            {this.props.registerError && this.props.registerError.Password && <p className="formError">{this.props.registerError.Password}</p>}
		    </label>
			<input
			    label="Password"
			    name="Password"
			    value={this.state.Password}
			    className="Password"
			    type="Password"
			    onChange={this.setText}
			/>
			<input type="submit" value="Register"/>
		</form>
		    <Link to="/login">Sign in</Link>
            </div>)}
	   </div>
        );
    }
}

function mapStateToProps(state) {
    let registerError = null;
    if (state.login.error && state.login.error.register) {
	const temp = state.login.error.register;
	if (temp.Username || temp.Name ||
	temp.Email || temp.Password) {
		registerError = temp;
	}
    }
    return {
	      error: state.login.error,
	      loginError: state.login.error && state.login.error.login,
	      registerError,
        user: state.login.user
    };
}

function mapDispatchToProps(dispatch) {
    return {
        login: creds => dispatch(login(creds)),
        register: creds => dispatch(register(creds)),
        logout: () => dispatch(logout()),
        ping: () => dispatch(ping())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
