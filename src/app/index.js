import React, { Component } from 'react';
import PlayerList from './PlayerList';
import Bar from './Bar';
import Selected from './SelectedPlayer';
import Login from './Login';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom'
import {connect} from 'react-redux';
import {fetch_all, clear_selected, search} from '../lib/footballers';
import {logout} from '../lib/login';

import './index.scss';

class App extends Component {
  render() {
    const isLoggedIn = !!this.props.user;
    return (
      <HashRouter>
        <div className="mainContainer">
          <Bar isLoggedIn={isLoggedIn} logout={this.props.logout} name={this.props.user && this.props.user.name}/>
          <Switch>
            <Route path="/selected">
              <Selected closeSelected={this.props.clear_selected} data={this.props.selected} />
            </Route>
            <Route type="login" path="/login" component={Login} />
            <Route type="register" path="/register" component={Login} />
            <Route path="/">
                {!isLoggedIn ?
                 <Redirect to="/login" /> :
                 <div className="contentHolder">
                     <PlayerList />
                     <Selected closeSelected={this.props.clear_selected} isBar={true} data={this.props.selected} />
                 </div>
                }
            </Route>
          </Switch>
        </div>
      </HashRouter>
    );
  }
}
function mapStateToProps(state) {
  return {
    selected: state.footballers.selected,
    user: state.login.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetch_all: () => dispatch(fetch_all()),
    clear_selected: () => dispatch(clear_selected()),
    search: (txt) => dispatch(search(txt)),
    logout: () => dispatch(logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
