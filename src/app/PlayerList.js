import React from 'react';
import {connect} from 'react-redux';
import cn from 'classnames';
import Helmet from 'react-helmet';

import PlayerCard from './PlayerCard';
import {fetch_selected, clear_selected, search, sort, group, sortOrder, toggleCollapse} from '../lib/footballers';
import {fetch_favourites, set_favourite, rem_favourite, toggleOnlyFavourites} from '../lib/footballers';
import {sortBy, groupBy} from 'lodash';

import './PlayerList.scss';

class PlayerList extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onTextChanged = this.onTextChanged.bind(this);
    this.onGroupingChanged = this.onGroupingChanged.bind(this);
    this.onSortChanged = this.onSortChanged.bind(this);
    this.onSortOrderChanged = this.onSortOrderChanged.bind(this);
    this.onlyFavourites = this.onlyFavourites.bind(this);
    this.state = {};
  }
  onTextChanged(event) {
    this.props.search(event.target.value);
  }
  onGroupingChanged(event) {
  	this.props.group(event.target.value);
  }
  onSortChanged(event) {
  	this.props.sort(event.target.value);
  }
  onSortOrderChanged(event) {
  	this.props.setSortOrder(event.target.value);
  }
  onlyFavourites(event) {
  	this.props.toggleOnlyFavourites();
  }
  componentDidMount() {
    this.props.search(this.props.searchText);  
    this.props.fetchFavourites();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
	const oldState = prevState || {};
	if (oldState.sortBy !== nextProps.sortBy || oldState.groupBy !== nextProps.groupBy ||
		oldState.sortOrder !== nextProps.sortOrder || oldState.footballers !== nextProps.footballers ||
		oldState.onlyFavourites !== nextProps.onlyFavourites) {
			const retState = {
				groupBy: nextProps.groupBy,
				sortBy: nextProps.sortBy,
				sortOrder: nextProps.sortOrder || 'd',
				footballers: nextProps.footballers,
				onlyFavourites: nextProps.onlyFavourites
			};
			if (nextProps.onlyFavourites) {
				retState.sortedList = sortBy(
					nextProps.footballers.filter(x => nextProps.favourites.includes(x.id)), 
					[nextProps.sortBy]);				
			} else {
				retState.sortedList = sortBy(nextProps.footballers, [nextProps.sortBy]);
			}
			
			if (retState.sortOrder === 'd') {
				retState.sortedList.reverse();
			}
			retState.groupedList = null;
			if (retState.groupBy !== 'None') {
			  	retState.groupedList  = groupBy(retState.sortedList, retState.groupBy);
			}
			return retState;
		}
		return null;
  }

  renderList(collection, key, collapsed = false) {
	return (	        <div key={key}  className="playerList">
	  	          <div className={cn("listContainer", { collapsed })}>
	  	            {
	  	              collection.map((x) => (
	  	                <PlayerCard 
	  	                  key={x.id} 
	  	                  data={x} 
	  	                  onClick={this.props.fetch_selected}
	  	                  isFavourite={this.props.favourites.includes(x.id)}
	  	                  setFavourite={this.props.setFavourite}
	  	                  unsetFavourite={this.props.unsetFavourite}
	  	                />
	  	              ))
	  	            }
	  	          </div>
	  	        </div>  	
	  	        );
  }

  renderGroup(obj) {
  	const res = [];
  	for (let elem in obj) {
		const isCollapsed = this.props.collapsedGroups.includes(elem);
  		res.push(
  		<div key={"groupElem - " + elem} className="groupElement">
  		<button className="groupTitle" onClick={() => this.props.toggleCollapse(elem)}>
  			<span>{elem}</span>
  			<i className="material-icons">{isCollapsed ? "keyboard_arrow_right" : "keyboard_arrow_down"}</i>
  		</button>
  		{this.renderList(obj[elem], "list - " + elem, isCollapsed)}
  		</div>);
  	}
  	return res;
  }
  
  render() {
    return (
      <div className="mainListContainer">
		<Helmet>
			<title>Home</title>
		</Helmet>
        <div className="searchDiv">
          <input onChange={this.onTextChanged} value={this.props.searchText} type="text" className="searchBox"/>
        </div>
        <div className="sortNGroup">
          <span>Group By: </span>
          <select value={this.props.groupBy}  onChange={this.onGroupingChanged}>
          	<option value="None">None</option>
          	<option value="Club">Club</option>
          	<option value="Nationality">Nationality</option>
          </select>
          <span>Sort By: </span>
          <select value={this.props.sortBy} onChange={this.onSortChanged}>
          	<option value="Rating">Rating</option>
          	<option value="Name">Name</option>
          	<option value="Age">Age</option>
           	<option value="Club">Club</option>
           	<option value="Nationality">Nationality</option>
          </select>
          <span>Sort Order: </span>
          <select value={this.state.sortOrder} onChange={this.onSortOrderChanged}>
            <option value="a">Asc</option>
            <option value="d">Desc</option>
          </select>
          <span>Only Favourites</span>
          <input type="checkbox" checked={this.state.onlyFavourites} onChange={this.onlyFavourites}/>
        </div>
          {
            this.props.error && (
                <div className="errorIndicator">
                  <p>
                    "Self Goal - Check server log :"
                  </p>
                  <br />
                  {`${JSON.stringify(this.props.error)}`}
                </div>
            )
          }
        {
        	this.state.groupedList ? (
				<div className="groupContainer">
  	        		{this.renderGroup(this.state.groupedList)}
        		</div>
        	) : (
        		this.renderList(this.state.sortedList, "default")
        	)
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    footballers: state.footballers.list,
    favourites: state.footballers.favourites,
    searchText: state.footballers.searchText,
    sortBy: state.footballers.sortBy,
    groupBy: state.footballers.groupBy,
    error: state.footballers.error,
    sortOrder: state.footballers.sortOrder,
    collapsedGroups: state.footballers.collapsedGroups,
    onlyFavourites: state.footballers.onlyFavourites
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetch_selected: (id) => dispatch(fetch_selected(id)),
    clear_selected: () => dispatch(clear_selected()),
    search: (txt) => dispatch(search(txt)),
    sort: (txt) => dispatch(sort(txt)),
    group: (txt) => dispatch(group(txt)),
    setSortOrder: (txt) => dispatch(sortOrder(txt)),
    toggleCollapse: (elem) => dispatch(toggleCollapse(elem)),
	fetchFavourites: () => dispatch(fetch_favourites()),
    setFavourite: (txt) => dispatch(set_favourite(txt)),
    unsetFavourite: (txt) => dispatch(rem_favourite(txt)),
    toggleOnlyFavourites: () => dispatch(toggleOnlyFavourites()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerList)
