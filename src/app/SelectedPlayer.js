import React from 'react';
import cn from 'classnames';
import Helmet from 'react-helmet';

import './SelectedPlayer.scss';
import {baseUrl, removeAccents} from '../utils';
import {Redirect} from 'react-router-dom';

export default class SelectedPlayer extends React.Component {
  render() {
    const data = this.props.data;
    return !data ? <Redirect to="/"/> : (
      <div className={cn('selectedView', { isBar: this.props.isBar })}>
      	{!this.props.isBar && 		<Helmet>
      				<title>{data.Name}</title>
      			</Helmet>}
        <button className="closeSelected" onClick={() => this.props.closeSelected()}>
			<i className="material-icons">close</i>
        </button>
        <img
          className="playerPicSel"
          alt={data.Name}
          // crossorigin="anonymous"
          src={`${baseUrl}/pic/${removeAccents(data.Name)}.png`}
        />
          <h3 className="titleSel">{data.Name}</h3>
        <div className="detailsMainSel">
          <div className="detailsChildSel">
		{
			Object.keys(data).map((x) => ["id", "createdAt", "updatedAt"].includes(x) ? null : (
				            <p key={x} className="detailSel">
					      <span className="cellsSel propertyName">{x}</span>
					      <span className="cellsSel">: {data[x]}</span>
					    </p>
			))
		}
          </div>
        </div>
      </div>
    );
  }
}
