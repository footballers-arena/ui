import React from 'react';
import {Link} from 'react-router-dom';

import './Bar.scss'

export default class Bar extends React.Component {
  render() {
    return (
      <div className="bar">
	<div className="leftContent">
	  <i className="material-icons homeLink">
	    <Link to="/">home</Link>
	  </i>
	</div>
	<div className="centerContent"/>
	<div className="rightContent">
	  { this.props.isLoggedIn && 
		  (<p className="nameUser">{this.props.name}</p> )}
	  { this.props.isLoggedIn && 
		  (<button className="exitLink" onClick={this.props.logout}>
		    <i className="material-icons" >
		     exit_to_app
		    </i>
		  </button>
		)
	  }
	</div>
      </div>
    );
  }
}
