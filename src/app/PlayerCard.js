import React from 'react'
import cn from 'classnames';
import {Redirect} from 'react-router-dom';

import './PlayerCard.scss';
import {removeAccents, baseUrl} from '../utils';

export default class PlayerCard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.cardClicked = this.cardClicked.bind(this);
      this.imageClicked = this.imageClicked.bind(this);
      this.toggleFavourite = this.toggleFavourite.bind(this);
    this.state = {
      redirect: false
    }
  }
    imageClicked() {
        this.props.onClick(this.props.data)
    }
  cardClicked() {
    this.props.onClick(this.props.data);
    this.setState({
      redirect: true
    })
  }
  toggleFavourite() {
  	if (this.props.isFavourite) {
  		this.props.unsetFavourite(this.props.data.id);
  	} else {
  		this.props.setFavourite(this.props.data.id);
  	}
  }
  render() {
    const data = this.props.data;
    return this.state.redirect ? <Redirect to="/selected"/> :(
      <div className={cn('card')} >
      	<button onClick={this.toggleFavourite} className={cn("favouriteCard", {nonFavourite: !this.props.isFavourite})}>
      		<i className="material-icons">{this.props.isFavourite ? "star" : "star_border" }</i>
      	</button>
        <img
          onClick={this.imageClicked}
          className="playerPic"
          alt={data.Name}
          src={`${baseUrl}/pic/${removeAccents(data.Name)}.png`}
        />
        <div className="detailsMain">
            <h3 className="title" onClick={this.cardClicked}>{data.Name}</h3>
          <div className="detailsChild">
            <p className="detail">
              <span className="cells">Rating</span>
              <span className="cells">: {data.Rating}</span>
            </p>
            <p className="detail">
              <span className="cells">Age</span>
              <span className="cells">: {data.Age}</span>
            </p>
            <p className="detail">
              <span className="cells">Club</span>
              <span className="cells">: {data.Club}</span>
            </p>
            <p className="detail">
              <span className="cells">Nationality</span>
              <span className="cells">: {data.Nationality}</span>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
